require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const { ApolloServer } = require("apollo-server-express");
const { typeDefs, resolvers } = require("./appFolder/schema");
const datasources = require("./appFolder/datasource");
const mongoose = require("mongoose");
const isAuth = require("./middleware/is-auth");
const cors = require("cors");
const corsOption = {
  origin: "http://localhost:3000"
};
const DBURI = process.env.DBURI;
const app = express();
mongoose
  .connect(DBURI, { useNewUrlParser: true })
  .then(() => {
    console.log("connected");
  })
  .catch(err => {
    console.log(`Error: ${err}`);
  });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOption));
app.use(isAuth);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, res }) => ({
    req,
    res,
    datasources
  }),
  graphiql: true
});

server.applyMiddleware({ app, cors: false, path: "/" });

const port = process.env.PORT || 3002;

app.listen(port, () => {
  console.log(`app running on ${port}`);
});
