const { gql } = require("apollo-server-express");

module.exports = gql`
  scalar Upload
  extend type Query {
    getPatients: [patientData]
    getSinglePatient(id: ID): patientData
    getUserPatients(userId: ID): [patientData]
    getAdmittedPatients(userId: ID): [patientData]
    getDischargedPatients(userId: ID): [patientData]
    getDeadPatients(userId: ID): [patientData]
    printRecord(code: String): patientData
  }

  extend type Mutation {
    createPatient(userId: ID!, data: patientInput): String!
    updatePatient(id: ID!, data: patientInput): String!
    deletePatient(id: ID): String!
    deleteAllPatient: String!
  }

  input patientInput {
    picture:Upload
    surName: String
    firstName: String
    lastName: String
    address: String
    nextOfKin: String
    gender: genders
    dateOfBirth: String
    code: String
    email: String
    phone: Int
    isAdmitted: String
    isDischarged: String
    isDead: String
  }

  enum genders {
    Male
    Female
  }

  type patientData {
    _id: ID
    picture:Upload
    surName: String
    firstName: String
    lastName: String
    address: String
    nextOfKin: String
    gender: String
    dateOfBirth: String
    code: String
    email: String
    phone: Int
    isAdmitted: String
    isDischarged: String
    isDead: String
    creator: userData
  }
`;
