const Base = require("../../../base");
const Patient = require("../../../model/patient/patient");
const User = require("../../../model/user/user");

class PatientAccount extends Base {
  constructor() {
    super("Patient Account");
  }

  //Mutation

  async createPatient(userId, data) {
    // this.isAuth;
    const user = await User.findById(userId);
    if (data.isAdmitted === toString(true)) {
      data.isAdmitted = true;
    }
    if (data.isDead === toString(true)) {
      data.isDead = true;
    }
    if (data.isDischarged === toString(true)) {
      data.isDischarged = true;
    }
    const code = Math.floor(Math.random() * 1000000);
    const newPatient = await new Patient({
      ...data,
      creator: user,
      code: user.username + "-" + code
    });

    newPatient.save();
    console.log(user);
    user.patients.push(newPatient);
    user.save();
    return `Your Code is ${newPatient.code}`;
  }

  async updatePatient(id, data) {
    const updated = await Patient.findByIdAndUpdate(id, data);
    updated.save();
    return "Patient Record updated";
  }

  async deletePatient(id) {
    const deleted = await Patient.findByIdAndRemove(id);
    console.log(deleted);
    return "Deleted";
  }

  async deleteAllPatient() {
    // const allEvent = await Event.find()
    const allDeleted = await Patient.deleteMany();
    console.log(allDeleted);
    return "all deleted";
  }

  //Query

  async getPatients() {
    const patientses = await Patient.find().populate("creator");
    return patientses;
  }

  async getSinglePatient(id) {
    const patient = await Patient.findById(id);
    return patient;
  }

  async getUserPatients(userId) {
    const user = await User.findById(userId);
    console.log(user);
    if (user._id.toString() === userId.toString()) {
      const patients = await Patient.find({ creator: user._id }).populate(
        "creator"
      );
      return patients;
    }
  }

  async getAdmittedPatients(userId) {
    const user = await User.findById(userId);
    console.log(user);
    if (user._id.toString() === userId.toString()) {
      const patients = await Patient.find({
        creator: user._id,
        isAdmitted: true
      }).populate("creator");
      return patients;
    }
  }

  async getDischargedPatients(userId) {
    const user = await User.findById(userId);
    console.log(user);
    if (user._id.toString() === userId.toString()) {
      const patients = await Patient.find({
        creator: user._id,
        isDischarged: true
      }).populate("creator");
      return patients;
    }
  }

  async getDeadPatients(userId) {
    const user = await User.findById(userId);
    console.log(user);
    if (user._id.toString() === userId.toString()) {
      const patients = await Patient.find({
        creator: user._id,
        isDead: true
      }).populate("creator");
      return patients;
    }
  }

  async printRecord(code) {
    if (code) {
      const patient = await Patient.findOne({ code: code }).populate("creator");
      return patient;
    } else {
      return "Record not found";
    }
  }
}

module.exports = PatientAccount;
