const PatientMutation = {
  createPatient: async (_, { userId, data }, { datasources, req }, info) => {
    // if (!req.isAuth) {
    //   throw new Error("Unauthorized");
    // }
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().createPatient(userId, data);
    console.log("sdsd");
    return response;
  },

  updatePatient: async (_, { id, data }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().updatePatient(id, data);
    return response;
  },

  deletePatient: async (_, { id }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().deletePatient(id);
    return "deleted";
  },

  deleteAllPatient: async (_, args, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().deleteAllPatient();
    return response;
  }
};

const PatientQuery = {
  getPatients: async (_, args, { datasources, req }, info) => {
    if (!req.isAuth) {
      throw new Error("Please Login to continue");
    }
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().getPatients();
    return response;
  },

  getSinglePatient: async (_, { id }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().getSinglePatient(id);
    return response;
  },

  getUserPatients: async (_, { userId }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().getUserPatients(userId);
    return response;
  },

  getAdmittedPatients: async (_, { userId }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().getAdmittedPatients(userId);
    return response;
  },

  getDischargedPatients: async (_, { userId }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().getDischargedPatients(userId);
    return response;
  },

  getDeadPatients: async (_, { userId }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().getDeadPatients(userId);
    return response;
  },

  printRecord: async (_, { code }, { datasources }, info) => {
    const { PatientAccount } = datasources;
    const response = await new PatientAccount().printRecord(code);
    return response;
  }
};

module.exports = { PatientMutation, PatientQuery };
