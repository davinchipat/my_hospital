const User = require("../../../model/user/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Base = require("../../../base");

class UserAccount extends Base {
  constructor() {
    super("User Account");
  }

  async createUser(data) {
    const existingUser = await User.findOne({ email: data.email });
    console.log(existingUser, "sdsd");

    if (existingUser) {
      throw new Error("User with this Email already exist");
    }

    console.log(data.password);

    data.password = await bcrypt.hash(data.password, 15);

    const newUser = await new User({ ...data });
    await newUser.save();
    return "Sign Up successful";
  }

  async login(data) {
    const currentUser = await User.findOne({ email: data.email });
    if (!currentUser) {
      throw new Error("User does not exist");
    }
    const valid = await bcrypt.compare(data.password, currentUser.password);
    if (!valid) {
      throw new Error("Invalid Password");
    }
    const token = jwt.sign(
      {
        id: currentUser.id,
        email: currentUser.email
      },
      "different",
      { expiresIn: "1h" }
    );
    return {
      userId: currentUser.id,
      token: token,
      expiration: 1
    };
  }

  async deleteUser(id) {
    await User.findByIdAndDelete(id);
    return "Deleted";
  }

  async deleteAllUser() {
    await User.remove();
    return "All Users Deleted";
  }

  async getUsers() {
    const users = await User.find({}).populate("patients");
    return users;
  }
}

module.exports = UserAccount;
