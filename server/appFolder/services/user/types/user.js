const { gql } = require("apollo-server-express");

module.exports = gql`
  extend type Query {
    getUsers: [userData]
  }

  extend type Mutation {
    createUser(data: userInput): String!
    login(data: loginInput): isAuth
    deleteUser(id: ID!): String
    deleteAllUser: String
  }

  input userInput {
    username: String!
    password: String!
    email: String!
  }

  input loginInput {
    password: String!
    email: String!
  }

  type isAuth {
    userId: ID!
    token: String!
    expiration: Int!
  }

  type userData {
    id: ID
    username: String
    password: String
    email: String
    patients: [patientData]
  }
`;
