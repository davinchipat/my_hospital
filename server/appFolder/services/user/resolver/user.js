const userMutation = {
    createUser: async(_, {data}, {datasources}, info) => {
        const {UserAccount} = datasources
        const response = await new UserAccount().createUser(data)
        return response
    },

    login: async(_, {data}, {datasources}, info) => {
        const {UserAccount} = datasources
        const response = await new UserAccount().login(data)
        return response
    },

    deleteUser: async(_, {id}, {datasources}, info) => {
        const {UserAccount} = datasources
        const response = await new UserAccount().deleteUser(id)
        return response
    },

    deleteAllUser: async(_, args, {datasources}, info) => {
        const {UserAccount} = datasources
        const response = await new UserAccount().deleteAllUser()
        return response
    }
}


const userQuery = {
    getUsers: async(_, args, {datasources}, info) => {
        const {UserAccount} = datasources
        const response = await new UserAccount().getUsers()
        return response
    }
}

module.exports = {userMutation,userQuery}