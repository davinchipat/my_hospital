const {GraphQLDateTime} = require('graphql-iso-date')
const {GraphQLJSON} = require('graphql-type-json')
const {Query, Mutation} = require('./resolvers')
const typeDefs = require('./types')

const resolvers = {
    Query,
    Mutation,
    DateTime: GraphQLDateTime,
    JSON: GraphQLJSON
}

module.exports = {typeDefs, resolvers}