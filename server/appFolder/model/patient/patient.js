const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const patientSchema = new Schema(
  {
    surName: String,
    firstName: String,
    lastName: String,
    address: String,
    nextOfKin: String,
    gender: {
      type: String,
      enum: ["Male", "Female"]
    },
    dateOfBirth: Date,
    code: String,
    email: String,
    phone: Number,
    isAdmitted: Boolean,
    isDischarged: Boolean,
    isDead: Boolean,
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user"
    }
  },
  {
    timestamps: true
  }
);

const Event = mongoose.model("patient", patientSchema);

module.exports = Event;
