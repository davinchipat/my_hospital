const { gql } = require("apollo-server-express");
const PatientSchema = require("./services/patient/type/patient");
const UserSchema = require("./services/user/types/user");

const linkSchema = gql`
  scalar JSON
  scalar DateTime

  type Query {
    _: Boolean
  }

  enum gender {
    male
    female
  }

  type Mutation {
    _: Boolean
  }
`;

module.exports = [linkSchema, PatientSchema, UserSchema];
