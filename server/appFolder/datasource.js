const PatientAccount = require("./services/patient/datasource/patient");
const UserAccount = require("./services/user/datasource/user");

module.exports = {
  PatientAccount,
  UserAccount
};
