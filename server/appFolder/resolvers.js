const {
  PatientMutation,
  PatientQuery
} = require("./services/patient/resolver/patient");
const { userMutation, userQuery } = require("./services/user/resolver/user");

const Query = {
  ...userQuery,
  ...PatientQuery
};

const Mutation = {
  ...userMutation,
  ...PatientMutation
};

module.exports = { Query, Mutation };
