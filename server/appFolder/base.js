const log = require('utils')
const Many = require('extends-classes')
const { AuthenticationError } = require( 'apollo-server-express' );
// const utils = require('../isAuth')

const { APP_NAME, dev} = process.env

class Base extends Many(){
    constructor(serviceName){
        super(serviceName)

        if(!serviceName || typeof serviceName !== "string"){
            throw new Error(`Expected a service Name but found ${serviceName}`)
        }

        this.serviceName = serviceName 
        this.rootDomain = dev ? "http://localhost:3001" : "https://server.la.ng"
    }

    async isLoggedIn( user ) {
        if ( !user ) {
          throw new AuthenticationError( 'Login to continue' );
        }
      }

    log(){
        return log.getLogger(`${APP_NAME}: ${serviceName}`)
    }
}

module.exports = Base